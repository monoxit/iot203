/*
   soil_moisture.ino An Arduino ESP8266 sample sketch.
   A battery powered moisture sensor for a plant pot sending LINE notification.
   LINE通知をするニッケル水素電池駆動の土壌水分センサ
   ESP8266 Core version >= v2.5.1 (w/ bearSSL)

   3 x NiMH AAA ->Diode->VCC
   IO16->RST->10k->VCC (Wakeup reset)
   IO4->3.9k-> NPN transister (2SC1815) base to switch sensor power
   IO14-> Soil moisture sensor digital output
   IO5-> LED

   Arduino IDEの新規タブでcredentials.h を追加し次の3行の#define分を追加。
   SSIDとWiFiパスワード, APIトークン部分は置き換える。

  #define WIFI_SSID "yourssid"
  #define WIFI_PASSWORD "wifipswd"
  #define LINE_API_TOKEN "line_notification_api_token"

   Copyright 2018 Masami Yamakawa (MONOxIT Inc.)
   This software is released under the MIT License.
   http://opensource.org/licenses/mit-license.php
*/

#include "credentials.h"
#include <ESP8266WiFi.h>
#include <WiFiClientSecure.h>

const char* ssid = WIFI_SSID;
const char* password = WIFI_PASSWORD;
const char* lineApiToken = LINE_API_TOKEN;

// LINEに通知するメッセージ
const String lineNotifyMessage = "ベンジャミン喉乾いた！";

// 次のPDFドキュメントに記載のステッカーの指定
// https://developers.line.me/media/messaging-api/sticker_list.pdf
// stickerPackage と stikerIdを0にするとステッカーなし
int stickerPackage = 1;
int stickerId = 401;

const char* host = "notify-api.line.me";
const String url = "/api/notify";
const int httpsPort = 443;

// LINE通知APIサーバ証明書の拇印の指定
const char* fingerprint = "5109d1b4bd0d6cbeeaf322504a33fa65ae6b5ee3"; //202008

// LEDが接続されているピン番号
const int ledPin = 5;

// 土壌水分センサの電源制御をするためのトランジスタが接続されているピン番号
const int moisturePowerPin = 4;

// 土壌水分センサのDO端子が接続さているピン番号
const int moistureInputPin = 14;

// マイコンに供給されている電源電圧の読み取りができる状態にする
ADC_MODE(ADC_VCC);

// SSL暗号通信を使うクライアントライブラリの準備をする
WiFiClientSecure client;

// 電源投入後一度だけ動く部分
void setup() {
  // put your setup code here, to run once:
  // WiFiモードを子機モードにする
  WiFi.mode(WIFI_STA);

  Serial.begin(115200);

  // LEDとセンサ電源制御ピンを出力モードに
  pinMode(ledPin, OUTPUT);
  pinMode(moisturePowerPin, OUTPUT);

  pinMode(moistureInputPin, INPUT);

  // 電源電圧を読み取り変数vに代入
  int v = ESP.getVcc();

  // センサの電源ON
  digitalWrite(moisturePowerPin, HIGH);

  // LEDを点灯
  digitalWrite(ledPin, HIGH);
  delay(3000);

  // センサの状態を読み取りdry変数に代入
  int dry = digitalRead(moistureInputPin);

  // センサ電源OFF
  digitalWrite(moisturePowerPin, LOW);
  // LED 消灯
  digitalWrite(ledPin, LOW);

  Serial.println("");
  Serial.print("DRY:");
  Serial.print(dry);
  Serial.print(" BATT:");
  Serial.println(v);

  // もしあらかじめdryに代入したセンサ値がHIGH(乾いている）か電源電圧が3000mV未満なら
  if (dry == HIGH || v < 3000) {
    Serial.println();
    Serial.print("connecting to ");
    Serial.println(ssid);

    // WiFiに接続開始
    WiFi.begin(ssid, password);

    // WiFiに接続できるまで訳10秒LEDを点滅させる
    for (int i = 0; WiFi.status() != WL_CONNECTED && i < 20; i++) {
      digitalWrite(ledPin, HIGH);
      delay(250);
      digitalWrite(ledPin, LOW);
      delay(250);
      Serial.print(".");
    }

    // もしWiFiに接続できなかったら約5分ディープスリープ
    if (WiFi.status() != WL_CONNECTED) {
      Serial.println("");
      Serial.println("[Error] WiFi not connected. Sleep...");
      ESP.deepSleep((unsigned long)(5 * 60 * 1000 * 1000)); // Sleep 5 min
    }

    Serial.println("");
    Serial.println("WiFi connected");
    Serial.println("IP address: ");
    Serial.println(WiFi.localIP());

    // サーバとSSLで接続時に拇印によるサーバ証明検証を実施するようにする
    client.allowSelfSignedCerts();
    client.setFingerprint(fingerprint);
    // 何らかの理由で接続相手検証を無効にしたいときは上2行をコメントアウトし下のclient.setInsecure()を有効にする
    //　client.setInsecure();

    // メッセージの入れ物を用意
    String message = "";

    // あらかじめdryに代入したセンサ値がHIGH(乾いている）なら
    if (dry == HIGH) {
      // メッセージにLINEに通知するメッセージを追加
      message += lineNotifyMessage;
    }

    // 電池電圧をメッセージに追加する
    message += "電池：";
    message += v;

    // lineNotifyの部分（プログラミングの用語では関数と呼ぶ）を呼び出しメッセージをLINEに送信する
    // パラメタには、通知したいメッセージとステッカーIDなどを指定する
    lineNotify(message, stickerPackage, stickerId);

    delay(1000);
    Serial.println("Sleep...");

    // 約1時間ディープスリープ
    ESP.deepSleep((unsigned long)(60 * 60 * 1000 * 1000));

  } else { // そうでない（dryがLOWでかつvが3000以上）なら
    Serial.println("Sleep...");
    // 約30分ディープスリープ
    ESP.deepSleep((unsigned long)(30 * 60 * 1000 * 1000));
  }
}

void loop() {
  // put your main code here, to run repeatedly:
}

// lineNotify部分
void lineNotify(String message, int stkpkgid, int stkid) {

  // 動作状態を確認しやすくするためにLEDを点灯
  digitalWrite(ledPin, HIGH);

  Serial.print("connecting to ");
  Serial.println(host);

  // 通知APIに接続を試しみて接続できなかったら
  if (!client.connect(host, httpsPort)) {
    Serial.println("[Error] connection failed. sleep...");
    char buf[256];
    // SSLエラーをシリアルモニタへ表示する
    client.getLastSSLError(buf, 256);
    Serial.print("SSL error: ");
    Serial.println(buf);

    // 約5分ディープスリープ
    ESP.deepSleep((unsigned long)(5 * 60 * 1000 * 1000));
  }

  Serial.print("requesting URL: ");
  Serial.println(url);

  // HTTPでAPIにPOSTするため、API規約に基づいた形でデータを構築する
  // API規約に基づいた送信メッセージ部分を作りlineMessageに代入する
  // 最終的にlineMessageは「message=ベンジャミン喉乾いた&stickerPackageId=1&stickerId=401」になる
  String lineMessage = "message=" + message;

  // API規約に基づいたステッカー部分をlineMessageに追加する
  if ((stkid != 0) && (stkpkgid != 0)) {
    lineMessage += "&stickerPackageId=";
    lineMessage += stkpkgid;
    lineMessage += "&stickerId=";
    lineMessage += stkid;
  }

  // HTTP ヘッダ部分を作る
  // Authorization: Bearer token123... の部分がユーザIDとパスワードの役割をするトークンが入る部分
  // \r\nは改行
  String header = "POST " + url + " HTTP/1.1\r\n" +
                  "Host: " + host + "\r\n" +
                  "User-Agent: ESP8266\r\n" +
                  "Authorization: Bearer " + lineApiToken + "\r\n" +
                  "Connection: close\r\n" +
                  "Content-Type: application/x-www-form-urlencoded\r\n" +
                  "Content-Length: " + lineMessage.length() + "\r\n";

  // HTTPヘッダをAPIサーバへ送信
  client.print(header);
  // 改行をAPIサーバへ送信
  client.print("\r\n");
  // ラインメッセージをAPIサーバへ送信
  client.print(lineMessage + "\r\n");

  Serial.print("request sent:");
  Serial.println(lineMessage);

  // APIサーバからの応答を待ち、応答されたメッセージをresに代入
  String res = client.readString();

  // resをシリアルモニタに出力
  Serial.println(res);

  // APIサーバとの接続を切断
  client.stop();
  // LED消灯
  digitalWrite(ledPin, LOW);
}
